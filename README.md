# Static Single Page Web Site

This container serves a single static web page.


## How to use this image

You can specify the page with either an argument when running the container or by mounting the static file as a volume.

## Argument

To provide the page as an argument, append it to the end of the `docker run` command:

```bash
docker run -p 80:80 americanreading/static '<h1>Hello, world</h1>'
```

## Volume

Or, mount a file as a volume mounted to `/usr/share/nginx/html/index.html`:

```bash
docker run -p 80:80 \
  -v $(pwd)/index.html:/usr/share/nginx/html/index.html:ro \
  americanreading/static
```
